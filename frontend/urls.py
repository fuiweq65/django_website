from django.urls import path
from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', views.PostList.as_view(), name = 'index'),
    path('about/', views.AboutPage, name='about'),
    path('login/', admin.site.urls, name='login'),
    path('<slug:slug>/', views.PostDetail.as_view(), name='post_detail'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)